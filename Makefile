# Makefile for slackupgrade 
# Copyright (C) 2019-2020 Sergey Poznyakoff.
#
# Slackware-upgrade-system is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackware-upgrade-system is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackupgrade.  If not, see
# <http://www.gnu.org/licenses/>.

PACKAGE   = slackupgrade
VERSION   = 2.3

SBINDIR   = /sbin
MANDIR    = /usr/man
MAN8DIR   = $(MANDIR)/man8
CONFDIR   = /etc/$(PACKAGE)
CONFFILES = conf/14.0-14.1.repl conf/14.1-14.2.repl conf/14.2-15.0.repl

help:;
	@echo "Avalable targets:"
	@echo ""
	@echo "$$ make install"
	@echo "   Installs the program.  Influential variables:"
	@echo "   SBINDIR - directory to install system binaries to ($(SBINDIR))"
	@echo "   MANDIR  - toplevel directory for man page installation ($(MANDIR))"
	@echo ""
	@echo "$$ make install-sbin"
	@echo "   Installs only the slackupgrade script."
	@echo ""
	@echo "$$ make install-man"
	@echo "   Installs only the manpage."
	@echo ""
	@echo "$$ make dist"
	@echo "   Creates the distribution tarball."

# ##########################
# Installation rules
# ##########################
install: install-sbin install-conf install-man

install-sbin:
	@if ! test -d $(DESTDIR)$(SBINDIR); then install -d $(DESTDIR)$(SBINDIR); fi
	install slackupgrade $(DESTDIR)$(SBINDIR)

install-man:
	@if ! test -d $(DESTDIR)$(MAN8DIR); then install -d $(DESTDIR)$(MAN8DIR); fi
	install -m 644 slackupgrade.8 $(DESTDIR)$(MAN8DIR)

install-conf:
	@if ! test -d $(DESTDIR)$(CONFDIR); then install -d $(DESTDIR)$(CONFDIR); fi
	install -m 644 $(CONFFILES) $(DESTDIR)$(CONFDIR)

# ##########################
# Distribution tarball rules
# ##########################

DISTDIR   = $(PACKAGE)-$(VERSION)
DISTFILES =\
 slackupgrade\
 slackupgrade.8\
 Makefile\
 COPYING\
 README\
 NEWS\
 $(CONFFILES)

distdir:
	test -d $(DISTDIR) || mkdir $(DISTDIR)
	tar -c -f - $(DISTFILES) | tar -C $(DISTDIR) -x -f -

dist: distdir
	tar zcf $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)
	@s="$(DISTDIR).tar.gz created"; \
	echo "$$s" | sed -e s/./=/g; \
	echo "$$s"; \
	echo "$$s" | sed -e s/./=/g

