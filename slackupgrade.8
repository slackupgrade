.\" This file is part of slackupgrade
.\" Copyright (C) 2019-2020 Sergey Poznyakoff.
.\"
.\" Slackware-upgrade-system is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License as published 
.\" by the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Slackware-upgrade-system is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with slackupgrade.  If not, see
.\" <http://www.gnu.org/licenses/>.
.TH SLACKUPGRADE 8 "February 7, 2020" "SLACKUPGRADE" "System Manager's Manual"
.SH NAME
slackupgrade \- do a full upgrade of a Slackware installation
.SH SYNOPSIS
.nh
.na
\fBslackupgrade\fR\
 [\fB\-aInSqvy\fR]\
 [\fB\-k \fIFILE\fR]\
 [\fB\-p \fIPACKAGE\fR\]\
 [\fB\-s \fISERIES\fR]\
 [\fIURL\fR]
.ad
.hy
.SH DESCRIPTION
Upgrades the Slackware installation to a new release.  The \fIURL\fR
argument supplies the URL of the Slackware distribution or the
directory on the local filesystem where it can be found.  If not
given, the program will use the nearest next version distribution
from
.nh
.BR https://mirrors.slackware.com .
.hy
.PP
When started, the program first verifies that it runs with root
privileges on a Slackware installation and determines the current
release version number.  Then it verifies the distribution \fIURL\fR:
it must contain the files \fBCHECKSUMS.md5\fR,
\fBANNOUNCE.\fIVERSION\fR and the Slackware package series
directories.
.PP
The file \fBCHECKSUMS.md5\fR and its GPG signature are downloaded
first.  Then, the program verifies that the signature is correct.
For this to succeed, you must have the Slackware Linux Project
public key in your GPG keyring.  If you don't, run
.PP
.EX
curl -o - https://www.slackware.com/gpg-key | gpg --import
.EE
.PP
When this initial check is passed, the program constructs two lists
of packages: a list of currently installed packages and a list of
packages available in the distribution.  The intersection of these two
is the list of \fIupgrade candidates\fR, i.e. packages that will be
upgraded.
.PP
The difference between these lists is the set of installed
packages that have no equivalent in the available package list.  Those
are \fIorphaned packages\fR, which were either removed from the Slackware
distribution, or were installed from third-party sources.  It is
unpredictable whether these will work on the newly upgraded
system, therefore they will be removed after a successful upgrade.
Before proceeding, the program will display this list on the screen
and save it in file 
.nh
\fB/var/log/slackupgrade-\fIOLD\fR-\fINEW\fB.removed\fR
.hy
for your consideration (here, \fIOLD\fR and \fINEW\fR stand for the
current and new Slackware version numbers, correspondingly).  After
the upgrade, you can re-install them, if necessary.
.PP
Known differences between Slackware releases are taken into account
when building lists of upgrade and delete candidates.  For example,
consider an upgrade from version 14.1 to 14.2.  It is known that the
\fIportmap\fR package from 14.1 is replaced with the \fIrpcbind\fR in
version 14.2.  Consequently, if the program sees that \fIportmap\fR is
installed on the system, it will include \fIrpcbind\fR to the list of
installation candidates. Information about package differences in
various versions is kept in \fIreplacement map files\fR.  See the
section \fBREPLACEMENT MAP\fR, for a discussion of these files.
.PP
In addition to upgrading already installed packages you can request
the program to install additional series or individual packages 
from the distribution with the \fB\-s\fR and \fB\-p\fR options.  E.g.,
it is often a good idea to install all packages from series \fBl\fR (ell),
like that: 
.PP
.EX
slackupgrade \-s l
.EE
.PP
Otherwise, you can request to install all missing packages from all
series, excepting \fBkde*\fR, by running the command with the
\fB\-a\fR option.
.PP
Once the package lists are ready, the program prints the current
Slackware version and the version it is going to upgrade to, and asks
you to confirm that you really want to upgrade.  This is the right
moment to quit if you decide to modify program invocation in order to
handle orphaned packages.  See the subsection
.B Handling of orphaned packages
for a discussion.
.PP
If you decide to continue and \fBslackupgrade\fR uses the remote
repository, it will select the upgrade mode to use.  There are two
possibilities:
.TP
.I Incremental mode
In this mode, the archive for each package will be downloaded
immediately prior to its installation and removed afterwards.  The
only exception are several basic packages that are needed for the
upgrade process and which will be downloaded and installed in the
first place.

This mode requires little additional disk space, but it is somewhat
risky.  If something happens to your internet connection during the
upgrade, you'll end up with partly upgraded (and consequently,
unstable) system.
.TP
.I Safe mode
In safe mode, \fBslackupgrade\fR first downloads all needed packages
from the remote repository and then proceeds to upgrade using local
disk copies.  Downloaded files are removed when no longer needed, i.e.
immediately after the corresponding package has been installed.

This mode requires around 2.5G of free disk space on the device which
hosts the \fBslackupgrade\fR spool directory (by default --
.BR /var/slackupgrade ).
You can select another location by setting the
\fBSLACKUPGRADE_PKGDIR\fR environment variable to the full pathname
of the spool directory you wish to use.

The advantage of the safe mode is that even if something goes wrong
during the upgrade, you have all the necessary stuff at hand to resume
upgrade from where you left off.
.PP
Safe mode is assumed if there is enough disk space.  Otherwise, the
program will ask you whether you wish to continue in incremental mode
and will act accordingly.
.PP
At the end of the run, the program prints additional instructions and
leaves the detailed log in file
.nh
\fB/var/log/slackupgrade-\fIOLD\fB\-\fINEW\fB.log\fR.
.hy
.sp
Here, \fIOLD\fR and \fINEW\fR stand for the old and new Slackware
release versions, correspondingly.
.PP
If any configuration files were created during the upgrade that
conflict with the existing files, they will be stored alongside the
original files, with the \fB.new\fR extension.  The list of these
files will be stored in file
\fB/var/log/slackupgrade\-\fIOLD\fB\-\fINEW\fB.new\fR.
.SS Handling of orphaned packages
There are two possible ways to handle installed packages that have no
installation candidate in the new Slackware release.

First, if you want to keep in place any of them, create a
\fIkeep-list\fR file.  This file is a list of package names, each on a
separate line.  When you start \fBslackupgrade\fR, use the
\fB\-k \fIFILE\fR\fR option to instruct it to use this file.
.PP
Secondly, if you are upgrading to the version for which there is no
replacement map, there can be replacement packages for some of the
orphaned ones.  You can create a replacement map and save it to
the \fB/etc/slackupgrade\fR directory (see the section
\fBREPLACEMENT MAP\fR for details).  If you do, please drop me a note
so that your changes become available for other users (see the
\fBBUGS\fR section, for contact information).  You can also use the
\fB\-p\fR command line option to provide the names of replacement
packages from the command line.
.SH OPTIONS
.TP
.B \-a
Install all series except \fBkde*\fR.
.TP
.B \-h
Display a short help summary and exit.
.TP
.B \-I
Force using incremental mode (see also \fB\-S\fR).
.TP
\fB\-k \fIFILE\fR
After successful upgrade, \fBslackupgrade\fR will remove
previously installed packages that are not available in the new
distribution.  This option allows you to supply a list of packages
that should not be removed.  Each line in \fIFILE\fR should list
exactly one package name, without version and architecture
information.  Empty lines and comments (\fB#\fR) are ignored.
.sp
To obtain initial list of packages that will be removed, run the
program with the \fB\-n\fR option.  At the end of the run, the list
will be stored in file
.nh
\fB/var/log/slackupgrade-\fIOLD\fR-\fINEW\fB.dry_run.removed\fR.
.hy
At a pinch (not really recommended), you can move it elsewhere and use
as argument to that option.  This way all packages will be preserved.
.TP
.B \-n
Dry-run mode: do nothing, print what would have been done.  In spite
of the name, the list of packages for removal will still be created.
.sp
In this mode, log files are created with the additional
\fB\.dry_run\fR suffix (see the section \fBFILES\fR).
.TP
\fB\-p \fIPACKAGE\fR
Install additional package.
.TP
.B \-q
Quiet mode: suppress all messages, except error diagnostics.
.TP
.B \-S
Force using safe mode.   Abort if not enough disk space is available.
.TP
\fB\-s \fISERIES\fR
Additionally install all packages from \fISERIES\fR.
.sp
You can use this option together with \fB\-a\fR to install entire
Slackware system, like that:
.sp
.nh
.na
slackupgrade -a -s kde -s kdei
.ad
.hy
.TP
.B \-v
Verbosely list each package being upgraded or removed.
.TP
.B \-y
Assume "yes" to all queries.
.SH REPLACEMENT MAP
Replacement map files contain names of packages that have been
replaced with another packages in the new release.  They are stored in
the \fB/etc/slackupgrade\fR directory.  Each replacement
map is named \fIOLD\fB\-\fINEW\fB.repl\fR, where \fIOLD\fR and
\fINEW\fR are two subsequent versions of Slackware.  For example, the
file \fB/etc/slackupgrade/14.1\-14.2.repl\fR contains
changes in package names between versions 14.1 and 14.2.
.PP
This version of \fBslackupgrade\fR is shipped with two
replacement maps, for upgrades between 14.0 and 14.1, and between 14.1
and 14.2.
.PP
A replacement map is a plaintext file with each package being described
on a separate line.  Lines consist of two or more columns separated
with any amount of whitespace.  Column one contains the name of the
package in the version \fIOLD\fR of Slackware.  Column two contains
the name of the corresponding package in Slackware version \fINEW\fR.
If the package was split into several packages, additional packages can
be listed on the same line.
.PP
Excessively long lines can be
split over several physical lines by using the traditional UNIX
approach, by ending each line that must be joined with the next one
with a backslash immediately followed by a newline character.
.PP
Comments are introduced with the hash sign (\fB#\fR) and extend to the
nearest newline character.  Empty lines and comments are ignored.
.SH FILES
.TP
\fB/etc/slackupgrade/\fIOLD\fB\-\fINEW\fB.repl\fR
Replacement map for upgrades from version \fIOLD\fR to \fINEW\fR.
.TP
.B /var/slackupgrade
Default spool directory.  This is where all downloaded files are
stored.  You can override the default location by setting the
.B SLACKUPGRADE_PKGDIR
environment variable.
.TP
\fB/var/log/slackupgrade\-\fIOLD\fB\-\fINEW\fB.log\fR
Detailed log of operations performed during the upgrade from version
\fIOLD\fR to \fINEW\fR.
.TP
\fB/var/log/slackupgrade\-\fIOLD\fB\-\fINEW\fB.new\fR
List of the new incoming config files on your system with the
\fB.new\fR extension.  You may need to merge them with your actual
files, or move them over, or simply remove them.  In any case, it is
good idea to carefully consider each of them.
.TP
\fB/var/log/slackupgrade-\fIOLD\fR-\fINEW\fB.removed\fR
The list of packages that were removed from the system.  Examine it.
You may need to re-install some or all of them from third-party
servers or from slackbuilds.
.PP
When running in dry run mode (see the \fB\-n\fR option), names of the
three log files above contain an additional \fB.dry_run\fR suffix:
.TP
\fB/var/log/slackupgrade\-\fIOLD\fB\-\fINEW\fB.dry_run.log\fR
.TP
\fB/var/log/slackupgrade\-\fIOLD\fB\-\fINEW\fB.dry_run.new\fR
.TP
\fB/var/log/slackupgrade-\fIOLD\fR-\fINEW\fB.dry_run.removed\fR
.SS Backups
At the start of each run, existing log files are backed up.  The name
of each backup is created by adding a tilde to the end of the log file
name.  E.g. \fBslackupgrade-14.1-14.2.log\fR is renamed to 
\fBslackupgrade-14.1-14.2.log~\fR.  Existing backup copies are renamed
using the following pattern: \fBX~\fR becomes \fBX~1\fR, \fBX~1\fR
becomes \fBX~2\fR and so on.  At most five backup copies are kept
(from \fBX~\fR up to \fBX~4\fR).
.SH ENVIRONMENT
.TP
.B SLACKUPGRADE_CONFDIR
Location of the configuration directory.  This is the place where
replacement maps are stored.  The default is
.BR /etc/slackupgrade .
.TP
.B SLACKUPGRADE_PKGDIR
Location of the \fBslackupgrade\fR package spool directory.  This is
the temporary storage for downloaded package tarballs.  The default is
.BR /var/slackupgrade .
.TP
.B SLACKUPGRADE_MIRRORS_URL
URL of the Slackware mirros site.  The default is
.BR https://mirrors.slackware.com/slackware .
.TP
.B TMP
Location of the temporary directory.  The default is
.BR /tmp .
.SH "SEE ALSO"
The \fBUPGRADE.TXT\fR document, outlining the procedure as a whole:
.nh
<\fBhttps://mirrors.nix.org.ua/linux/slackware/slackware\-14.1/UPGRADE.TXT\fR>.
.hy
.PP
.BR upgradepkg (8).
.SH BUGS
Only main Slackware packages are considered.  The \fBpatches\fR
subdirectory is not used.
.PP
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2019 \(em 2020 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later
<http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

